package ioc;

public class Data{
    private String value;

    public Data(String text){
        this.value = text;
    }

    public String getValue() {
        return this.value;
    }

}